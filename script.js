// This is here as a default before anything else is loaded.
var defaultBook = {
  "start": "start",
  "pages": {
    "start": {
      "text": "Which flag do you want to see?",
      "image": "https://pbs.twimg.com/media/EyWXvd3XEAIbbRn?format=jpg",
      "choices": [
        { "text": "Canada", "page": "ca" },
        { "text": "Germany", "page": "de" },
        { "text": "Trans", "page": "traa" },
        { "text": "None", "page": "none" }
      ]
    },
    "ca": {
      "image": "https://pbs.twimg.com/media/E4TdELIWQAMsfBM?format=jpg"
    },
    "de": {
      "image": "https://pbs.twimg.com/media/EGO90PZW4AAaBD9?format=jpg"
    },
    "traa": {
      "text": "Well, that isn't a country, but OK!",
      "image": "https://pbs.twimg.com/media/EEdBotsWkAYuGje?format=jpg"
    },
    "none": {
      "text": "OK then... here's nothing!",
      "choices": [
        { "text": "Actually, Canada is cool...", "page": "ca" }
      ]
    }
  }
}

// This class keeps track of the actual game:
class GameState {
  constructor (book) {
    this.book = book
    this.reset()
  }

  reset () {
    this.page = this.book.start || 'start'
  }

  goTo (page) {
    if (!Object.keys(this.book.pages || {}).includes(page)) { return false }

    this.page = page
    return true
  }

  currentPage () {
    return (this.book.pages || {})[this.page]
  }
}

// This class serves as glue between the HTML elements and the game itself:
class PageManager {
  constructor (selector, book) {
    this.elem = document.querySelector(selector)
    this.load(book)
  }

  load (book) {
    this.player = new GameState(book)
    this.draw()
  }

  reset () {
    this.player.reset()
    this.draw()
  }

  handleChoice (page) {
    this.player.goTo(page)
    this.draw()
  }

  draw () {
    this.elem.innerHTML = ''
    const page = this.player.currentPage()
    if (!page) { return }

    if (page.text) {
      const text = this.elem.appendChild(document.createElement('div'))
      text.className= 'cyoa-text'
      text.textContent = page.text
    }

    if (page.choices) {
      const choices = this.elem.appendChild(document.createElement('div'))
      choices.className= 'cyoa-choices'

      page.choices.forEach(item => {
        const choice = choices.appendChild(document.createElement('button'))
        choice.className= 'cyoa-choice'
        choice.textContent = item.text
        choice.onclick = () => this.handleChoice(item.page)
      })
    }

    if (page.image) {
      const image = this.elem.appendChild(document.createElement('img'))
      image.className= 'cyoa-image'
      image.src = page.image
    }
  }
}

window.onload = function () {
  // Set up a page manager to handle the actual logic:
  const pageManager = new PageManager('#player', defaultBook)
  document.querySelector('#reset').onclick = () => pageManager.reset()

  // Allow users to select a file on their machine to play instead:
  document.querySelector('#upload').onchange = (event) => {
    const reader = new FileReader()
    reader.onload = () => {
      try { pageManager.load(JSON.parse(reader.result)) }
      catch (error) { alert(["Couldn't load file:", error.message].join(' ')) }
    }
    reader.readAsText(event.target.files[0])
  }
}
